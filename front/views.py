from django.shortcuts import render, HttpResponse
from steppers.stepper import xy_stepper,CameraMicroscope,Scanner
from django.views.decorators.csrf import csrf_exempt
import threading

stepper = xy_stepper()
run_thread = threading.Thread(target=stepper.run)


def main(request):
    """Render account page"""
    return render(request, 'front/home.html')

@csrf_exempt
def forward_y(request):
    stepper.forward()
    return render(request, 'front/home.html')

@csrf_exempt
def backward_y(request):
    stepper.backward()
    return render(request, 'front/home.html')

@csrf_exempt
def forward_x(request):
    stepper.right()
    return render(request, 'front/home.html')

@csrf_exempt
def bacward_x(request):
    stepper.left()
    return render(request, 'front/home.html')

@csrf_exempt
def home(request):
    stepper.home()
    return render(request, 'front/home.html')

@csrf_exempt
def run(request):
    if not stepper.pause_stat:
        try:
            run_thread.start()
        except:
            stepper.run()
    else:
        stepper.run()
    return render(request, 'front/home.html')

@csrf_exempt
def stop(request):
    stepper.stop()
    return render(request, 'front/home.html')

@csrf_exempt
def pause(request):
    stepper.pause()
    return render(request, 'front/home.html')

@csrf_exempt
def init(request):
    if request.GET.get('str') == 'full':
        stepper.scanner.strategy = True
    else:
        stepper.scanner.strategy = False
    if request.GET.get('int') == 'no':
        stepper.scanner.interrupt = False
    else:
        stepper.scanner.interrupt = True
    return render(request, 'front/home.html')

@csrf_exempt
def test(request):
    stepper.camera.image_path = request.GET.get('path')
    stepper.camera.magnification = request.GET.get('mag')
    stepper.STEP_X = int(request.GET.get('xs'))
    stepper.STEP_Y = int(request.GET.get('ys'))
    stepper.frame_skip = int(request.GET.get('fs'))
    return render(request, 'front/home.html',{"stepper":stepper})