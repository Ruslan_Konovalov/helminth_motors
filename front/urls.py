from django.urls import path
from . import views


app_name = 'front'
urlpatterns = [
    path('', views.main, name='main'),
    path('forward_x/',views.forward_x),
path('forward_y/',views.forward_y),
path('backward_x/',views.bacward_x),
path('backward_y/',views.backward_y),
path('home/',views.home),
path('run/',views.run),
path('stop/',views.stop),
    path('pause/',views.pause),
    path('test/',views.test),
    path('set/',views.init),

]
