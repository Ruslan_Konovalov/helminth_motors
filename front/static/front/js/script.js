$(document).ready(function(){

	$(".control-box").on("click", ".control", function(){
		var action = $(this).attr("data-control");
		console.log(action); //get data value, can send on server via ajax
	});

	$(".radio-btn").on("change", function() {
		var action = $(this).val();
		var name = $(this).attr("name")
		console.log(name, action); //get data value, can send on server via ajax
	});




	$("#settings-btn").on("click", function() {//show settings
		$("body").removeClass("show-window").addClass("show-window");
	});

	$("#overlay").on("click", function(e) {//hide settings
		var container = $("#form-settings");

		if (container.has(e.target).length === 0){
			$("body").removeClass("show-window");
		};
	});
	
	$("#form-settings-close").on("click", function(e) {//hide settings
		$("body").removeClass("show-window");
	});

	$("#form-settings").on("submit", function(){
		var data = $(this).serializeArray();
		console.log(data);//get data value, can send on server via ajax
		return false;
	});
});