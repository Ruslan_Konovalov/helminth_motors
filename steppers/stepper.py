from steppers.scanner import new_go
from steppers.scanner import home as xy_home
from steppers.stepper_pref import preferences
from itertools import cycle

class Scanner():
    def __init__(self):
        self.strategy = True
        self.interrupt = True

class CameraMicroscope():
    def __init__(self):
        self.image_path = None
        self.magnification = 0

    def photo(self):
        ## Заменить на функцию которая делает фотографии и анализирует их

        print("TAKE PHOTO")


class xy_stepper():
    def __init__(self):
        self.IS_RUNNUNG = False
        self.current_x = 0
        self.current_y = 0
        self.local_x = 0
        self.local_y = 0
        self.pause_stat = False
        self.frames = 0
        self.eggs = 0
        self.init_steppers(preferences.STEP_X,preferences.STEP_Y,preferences.MAX_X,preferences.MAX_Y)
        self.scanner = Scanner()
        self.camera = CameraMicroscope()
        self.frame_skip = 0

    def init_steppers(self,STEP_X, STEP_Y, MAX_X, MAX_Y):
        self.STEP_X = STEP_X
        self.STEP_Y = STEP_Y
        self.MAX_X = MAX_X
        self.MAX_Y = MAX_Y

    def reload(self):
        self.IS_RUNNUNG = False
        self.current_x = 0
        self.current_y = 0
        self.local_x = 0
        self.local_y = 0
        self.pause_stat = False
        self.frames = 0
        self.eggs = 0

    def runtil(self, val_x, val_y):
        real_d_x = cycle(self.x_d)
        real_d_y = cycle(self.y_d)
        while next(real_d_x) != val_x:
            pass
        while next(real_d_y) != val_y:
            pass
        return real_d_x, real_d_y

    def forward(self):
        new_go('w', self.STEP_Y)
        print('FORWARD')

    def backward(self):
        new_go('s', self.STEP_Y)
        print('BACKWARD')

    def left(self):
        new_go('a', self.STEP_X)
        print("LEFT")

    def right(self):
        new_go('d', self.STEP_X)
        print("RIGHT")

    def pause(self):
        print('PAUSE')
        self.IS_RUNNUNG = False
        while not self.pause_stat:
            pass
        self.current_x = self.local_x
        self.current_y = self.local_y
        print("Coords = {} {}".format(self.current_x, self.current_y))

    def stop(self):
        self.IS_RUNNUNG = False
        self.home()
        print("STOP")
        self.reload()

    def run(self):
        self.IS_RUNNUNG = True
        print('RUN START', self.IS_RUNNUNG)
        self.scan()
        print("RUN END")

    def home(self):
        xy_home('x')
        xy_home('y')
        self.reload()
        print("Home")

    def take_photo(self):
        self.camera.photo()
        self.frames += 1
        return True ## Сейчас функция возвращает всегда True, она должна возвращать результат поиска

    def scan(self):
        print('scan start', self.current_x, self.current_y, self.MAX_X, self.MAX_Y, )
        if self.pause_stat:
            self.pause_stat = False
            print("Start from pause")
        print(self.IS_RUNNUNG)
        for i in range(0, self.MAX_X, self.STEP_X):
            if self.IS_RUNNUNG:
                for j in range(0, self.MAX_Y, self.STEP_Y):
                    print('--', i, j, self.current_x, self.current_y, '--')
                    if not self.scanner.strategy and self.frames%self.frame_skip == 0:
                        pass
                    elif self.IS_RUNNUNG:
                        if (((i == self.current_x) and (j >= self.current_y)) or (i > self.current_x)):
                            if i % 2 == 0:
                                new_go('w', self.STEP_Y)
                            else:
                                new_go('s', self.STEP_Y)
                            print('Ats->', i, j)
                            data = self.take_photo()
                            if self.scanner.interrupt and data:
                                return
                            self.local_x = i
                            self.local_y = j
                    else:
                        break
                new_go('d', self.STEP_X)
            else:
                break
        self.pause_stat = True
