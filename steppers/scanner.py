import RPi.GPIO as GPIO
import time
import sys
import os


GPIO.setmode(GPIO.BOARD) #Set Raspberry Pi GPIO to BOARD numbering (as opposed to BCM)

ControlPin = {'x':[29,31,33,35],
		'y':[37,36,38,40]}

for pin in ControlPin['x']:
	GPIO.setup(pin, GPIO.OUT) #Set each pin to the OUTPUT mode
	GPIO.output(pin,0) #Make sure they start as Off
for pin in ControlPin['y']:
	GPIO.setup(pin, GPIO.OUT)
	GPIO.output(pin,0)

GPIO.setup(7, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) #Keeping this for when I add sensors / limit switches, useless for now

x_lim = 0. #using type float() so we don't lose degrees over time from rounding
y_lim = 0.

def checklimit(axis, needs = True): #Returns true or false during go() to signal that the global limits are within/outside their set range
	if needs:
		if axis == 'x':
			return x_lim < 0
		else:
			return y_lim < 0
	else:
		return False


def updatelim(dir,axis): #update current angle of global limit vars based on current vector
	global x_lim
	global y_lim
	if dir == 1: #first number of sequence handed to updatelim is either 1 or 0 (forward or backwards) this determins direciton of vector
		x = 1
	if dir == 0:
		x = -1
	if axis == 'x':
		x_lim = x_lim + (.703125 * x) #these motors have 512 steps per revolution, each step is .703125 degrees
	if axis == 'y':
		y_lim = y_lim + (.703125 * x)

def go(seq,steps,speed,axis,needs = True): #Takes the forward or backwards sequence, number of steps, delay between steps in microseconds, and axis to move the appropriate motors the desired number of steps
	global x_lim
	global y_lim
	for i in range(steps): #in a typical keypress, this is 1
		updatelim(seq[1][1],axis) #checks the first number of the current sequence to determine whether this is forward or backwards (see updatelim)
		if checklimit(axis,needs) == True: #If the axis is outside it's limits, nope nope nope nope.
			updatelim(1 - seq[1][1],axis)
			break
		for halfstep in range(8): #the sequences of 1's and 0's later on (f/b sequences) represent the on/off state of each output pin for a given axis. we have to switch this 8 times just to do one step
			for pin in ControlPin[axis]: #for each one of THOSE 8 sequences, we change the four output pins accordingly
				GPIO.output(pin, seq[halfstep][ControlPin[axis].index(pin)])
			time.sleep(float(speed)/1000) #we're moving a physical metal shaft here, so let's sleep and let it have a bit of time to catch up to what we're outputting, or else the motor will skip steps and not move anywhere.

def home(axis):
	global x_lim
	global y_lim
	if axis == 'x': #pretty simple. Monitor x and y axis angles until each is at 180 (striaght up).
		while x_lim  > 0:
				go(b,1,2,axis)
	if axis == 'y':
		while y_lim > 0:
				go(b,1,2,axis)

def compile(log,mode):
	if mode == 1:
		workfile = open('recording.txt', 'w')
		multiplier = 1
		for number, action in enumerate(log):
			if isinstance(action, int):
				workfile.write("time.sleep(" + str(action) + ")\n")
				continue
			elif action == 'x':
				break
			elif action == 'h':
				workfile.write(keylog[action])
			elif action == log[number+1]: #if action = next action
				multiplier += 1 #We're going to need multiples!
				continue
			if multiplier > 1:
				workfile.write(keylog[action][0] + str(keylog[action][1]*multiplier) + keylog[action][2])
				multiplier = 1
			else:
				workfile.write(keylog[action][0] + str(keylog[action][1]) + keylog[action][2])
		workfile.close()

f = [ [1,0,0,0], #each group in this sequence represents the state of four output pins.
	[1,1,0,0], #imagine the diagonal shape of 1's as the direction we're pushing magnetic current
	[0,1,0,0],
	[0,1,1,0],
	[0,0,1,0],
	[0,0,1,1],
	[0,0,0,1],
	[1,0,0,1] ]

b = [ [0,0,0,1],
	[0,0,1,1],
	[0,0,1,0],
	[0,1,1,0],
	[0,1,0,0],
	[1,1,0,0],
	[1,0,0,0],
	[1,0,0,1] ]


def new_go(vector,steps):
	seq, _, speed, axis = keymap.get(vector)
	go(seq,steps,speed,axis)

log = False

keymap = {'w':[b,1,2,'y'], #Created a dictionary of all key mappings, and direciton/steps/speed/axis for each key.
	  's':[f,1,2,'y'], #I've created more keys for a "two handed layout" to make moving the gimal in either 1 small step, or one step of 45 degrees.
	  'a':[b,1,2,'x'],
	  'd':[f,1,2,'x'],
	  'i':[b,64,2,'y'],
	  'k':[f,64,2,'y'],
	  'j':[b,64,2,'x'],
	  'l':[f,64,2,'x']}

keylog = {'w':["go(b,",1,",2,'y')\n"], #These are the strings put into a recording file.
	  's':["go(f,",1,",2,'y')\n"],
	  'a':["go(b,",1,",2,'x')\n"],
	  'd':["go(f,",1,",2,'x')\n"],
	  'i':["go(b,",64,",2,'y')\n"],
	  'k':["go(f,",64,",2,'y')\n"],
	  'j':["go(b,",64,",2,'x')\n"],
	  'l':["go(f,",64,",2,'x')\n"],
	  'h':["home('x')\nhome('y')\n"],
	  'x':["\n"]}