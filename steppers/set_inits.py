from stepper import backward,forward,left,right, xy_home
from test import new_go
import time



print("Hello, let's start calibration")
print("First calibrate X-axis")
flag = True
while flag:
    print("Move X-stepper to 0, use +/- to rotate, s - to save")
    sl = input()
    for s in sl:
        if s=='+':
            new_go('w',10)
        elif s == '-':
            new_go('s',10)
        elif s == 's':
            flag = False



MAX_X = 0

flag = True
while flag:
    print("Move X-stepper to MAX, use + to rotate, s - to save")
    sl = input()
    for s in sl:
        if s=='+':
            MAX_X +=1
            new_go('w',1)
        elif s == 's':
            flag = False

print("MAX X VAL= {}".format(MAX_X))

flag = True
while flag:
    print("Move Y-stepper to 0, use +/- to rotate, s - to save")
    sl = input()
    for s in sl:
        if s=='+':
            new_go('d',10)
        elif s == '-':
            new_go('a',10)
        elif s == 's':
            flag = False


MAX_Y = 0

flag = True
while flag:
    print("Move Y-stepper to MAX, use + to rotate, s - to save")
    sl = input()
    for s in sl:
        if s=='+':
            MAX_Y +=1
            new_go('d',1)
        elif s == 's':
            flag = False
print("MAX Y VAL= {}".format(MAX_Y))
