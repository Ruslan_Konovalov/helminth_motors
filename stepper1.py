
from steppers.scanner import  new_go
from steppers.scanner import home as xy_home
from stepper_pref import STEP,MAX_X,MAX_Y
import time
from itertools import cycle


class xy_stepper():
    def __init__(self):
        self.IS_RUNNUNG = False
        self.current_x = 0
        self.current_y = 0
        self.local_x = 0
        self.local_y = 0
        self.pause_stat = False
        self.frames = 0
        self.eggs = 0
        self.x_d = [i for i in range(0,MAX_X,STEP)]
        self.y_d = [i for i in range(0,MAX_Y,STEP)]


    def runtil(self,val_x,val_y):
        real_d_x = cycle(self.x_d)
        real_d_y = cycle(self.y_d)
        while next(real_d_x)!=val_x:
            pass
        while next(real_d_y)!=val_y:
            pass
        return real_d_x,real_d_y
    def forward(self):
        new_go('w',STEP)
        print('FORWARD')

    def backward(self):
        new_go('s', STEP)
        print('BACKWARD')

    def left(self):
        new_go('a', STEP)
        print("LEFT")

    def right(self):
        new_go('d', STEP)
        print("RIGHT")

    def pause(self):
        print('PAUSE')
        self.IS_RUNNUNG = False
        while not self.pause_stat:
            pass
        self.current_x = self.local_x
        self.current_y = self.local_y
        print("Coords = {} {}".format(self.current_x,self.current_y))

    def stop(self):
        self.IS_RUNNUNG = False
        self.home()
        print("STOP")

    def run(self):
        self.IS_RUNNUNG = True
        print('RUN START',self.IS_RUNNUNG)
        self.scan()
        print("RUN")

    def home(self):
        xy_home('x')
        xy_home('y')
        print("Home")

    def take_photo(self):
        print("Photo")
        time.sleep(0.5)
        self.frames += 1

    def scan(self):
        print('scan start',self.current_x,self.current_y,MAX_X,MAX_Y,)
        if self.pause_stat:
            self.pause_stat = False
            print("Start from pause")
        print(self.IS_RUNNUNG)
        for i in range(0,MAX_X,STEP):
            if self.IS_RUNNUNG:
                for j in range(0,MAX_Y,STEP):
                    print('--',i,j,self.current_x,self.current_y,'--')
                    if self.IS_RUNNUNG:
                        if (((i == self.current_x) and (j >= self.current_y)) or (i > self.current_x)):
                            if i%2==0:
                                new_go('w',STEP)
                            else:
                                new_go('s',STEP)
                            print('Ats->',i,j)
                            self.take_photo()
                            self.local_x = i
                            self.local_y = j
                    else:
                        break
                new_go('d',STEP)
            else:
                break
        self.pause_stat = True
